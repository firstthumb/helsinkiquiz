package com.ekocaman.interview;

import org.joda.time.DateTime;

import java.util.SortedSet;

public class Conversation implements Comparable<Conversation> {
    private String id;
    private SortedSet<Message> messages;
    private DateTime createdAt;

    public Conversation(String id, SortedSet<Message> messages, DateTime createdAt) {
        this.id = id;
        this.messages = messages;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public SortedSet<Message> getMessages() {
        return messages;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public int compareTo(Conversation o) {
        if (o.getCreatedAt() == null || createdAt == null) {
            return 0;
        }

        return (int) (o.getCreatedAt().getMillis() - createdAt.getMillis());
    }
}