package com.ekocaman.interview;

import org.joda.time.DateTime;

public class Message implements Comparable<Message> {
    private String id;
    private String body;
    private String fromUserId;
    private DateTime createdAt;

    public Message(String id, String body, String fromUserId, DateTime createdAt) {
        this.id = id;
        this.body = body;
        this.fromUserId = fromUserId;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public int compareTo(Message o) {
        return (int) (o.getCreatedAt().getMillis() - createdAt.getMillis());
    }
}