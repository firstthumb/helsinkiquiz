package com.ekocaman.interview;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class ConversationService {
    public static final String API_BASE_URL = "https://static.everyplay.com/developer-quiz/data";

    private final Gson gson = new Gson();
    private final RestClient restClient;

    public ConversationService(RestClient restClient) {
        this.restClient = restClient;
    }

    public String getRecentConversationSummaries() throws UnirestException {
        final TreeSet<Conversation> conversations = new TreeSet<Conversation>();
        final Map<String, User> users = getUsers();

        HttpResponse<JsonNode> conversationsResponse = restClient.doGetRequest(API_BASE_URL + "/conversations");

        if (conversationsResponse != null && conversationsResponse.getStatus() == 200) {
            JSONArray conversationArray = conversationsResponse.getBody().getArray();
            for (int i = 0; i < conversationArray.length(); i++) {
                JSONObject conversationJson = conversationArray.getJSONObject(i);
                String conversationId = conversationJson.getString("id");
                SortedSet<Message> messages = getMessages(conversationId);

                conversations.add(new Conversation(conversationId, messages, messages.isEmpty() ? null : messages.first().getCreatedAt()));
            }
        }
        // Create response
        List<ConversationResponse> conversationResponses = new ArrayList<ConversationResponse>();
        for (Conversation conversation : conversations) {
            if (!conversation.getMessages().isEmpty()) {
                Message message = conversation.getMessages().first();
                MessageResponse messageResponse = new MessageResponse();
                messageResponse.setId(message.getId());
                messageResponse.setBody(message.getBody());
                messageResponse.setCreatedAt(message.getCreatedAt().toString());

                User user = users.get(message.getFromUserId());
                if (user != null) {
                    messageResponse.setUser(new UserResponse(user.getId(), user.getAvatar()));
                }

                ConversationResponse conversationResponse = new ConversationResponse();
                conversationResponse.setId(conversation.getId());
                conversationResponse.setLastMessage(messageResponse);
                conversationResponses.add(conversationResponse);
            }
        }

        return gson.toJson(conversationResponses);
    }


    public Map<String, User> getUsers() throws UnirestException {
        final Map<String, User> users = new HashMap<String, User>();

        HttpResponse<JsonNode> userResponse = restClient.doGetRequest(API_BASE_URL + "/users");

        if (userResponse != null && userResponse.getStatus() == 200) {
            JSONArray userArray = userResponse.getBody().getArray();
            for (int i = 0; i < userArray.length(); i++) {
                JSONObject userJson = userArray.getJSONObject(i);
                String userId = userJson.getString("id");
                users.put(userId, new User(userId, userJson.getString("username"), userJson.getString("avatar_url")));
            }
        }

        return users;
    }

    public SortedSet<Message> getMessages(String conversationId) throws UnirestException {
        final TreeSet<Message> messageTree = new TreeSet<Message>();

        HttpResponse<JsonNode> messageResponse = restClient.doGetRequest(API_BASE_URL + "/conversations/" + conversationId + "/messages");

        if (messageResponse != null && messageResponse.getStatus() == 200) {
            JSONArray messageArray = messageResponse.getBody().getArray();
            for (int j = 0; j < messageArray.length(); j++) {
                JSONObject messageJson = messageArray.getJSONObject(j);

                Message message = new Message(
                        messageJson.getString("id"),
                        messageJson.getString("body"),
                        messageJson.getString("from_user_id"),
                        DateTime.parse(messageJson.getString("created_at"))
                );
                messageTree.add(message);
            }
        }

        return messageTree;
    }
}
