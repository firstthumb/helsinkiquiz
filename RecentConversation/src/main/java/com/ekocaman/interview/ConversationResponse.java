package com.ekocaman.interview;

import com.google.gson.annotations.SerializedName;

public class ConversationResponse {
    private String id;
    @SerializedName("latest_message")
    private MessageResponse lastMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MessageResponse getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(MessageResponse lastMessage) {
        this.lastMessage = lastMessage;
    }
}