package com.ekocaman.interview;

import com.google.gson.annotations.SerializedName;

public class UserResponse {
    private String id;
    @SerializedName("avatar_url")
    private String avatar;

    public UserResponse() {
    }

    public UserResponse(String id, String avatar) {
        this.id = id;
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}