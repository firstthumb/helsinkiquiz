package com.ekocaman.interview;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConversationServiceTest {

    private final RestClient restClient = new RestClient();
    private final Gson gson = new Gson();

    @Test
    public void testFetchRecentConversationSuccessfully() throws UnirestException {
        ConversationService conversationService = new ConversationService(restClient);
        String response = conversationService.getRecentConversationSummaries();

        JsonArray jsonArray = gson.fromJson(response, JsonArray.class);

        assertEquals(jsonArray.get(0).getAsJsonObject().get("id").getAsString(), "1");
        assertEquals(jsonArray.get(0).getAsJsonObject().get("latest_message").getAsJsonObject().get("id").getAsString(), "1");
        assertEquals(jsonArray.get(0).getAsJsonObject().get("latest_message").getAsJsonObject().get("body").getAsString(), "Moi!");
        assertEquals(jsonArray.get(0).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("id").getAsString(), "1");
        assertEquals(jsonArray.get(0).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("avatar_url").getAsString(), "http://placekitten.com/g/300/300");
        assertEquals(jsonArray.get(0).getAsJsonObject().get("latest_message").getAsJsonObject().get("created_at").getAsString(), "2016-08-25T10:15:00.670Z");

        assertEquals(jsonArray.get(1).getAsJsonObject().get("id").getAsString(), "2");
        assertEquals(jsonArray.get(1).getAsJsonObject().get("latest_message").getAsJsonObject().get("id").getAsString(), "2");
        assertEquals(jsonArray.get(1).getAsJsonObject().get("latest_message").getAsJsonObject().get("body").getAsString(), "Hello!");
        assertEquals(jsonArray.get(1).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("id").getAsString(), "3");
        assertEquals(jsonArray.get(1).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("avatar_url").getAsString(), "http://placekitten.com/g/302/302");
        assertEquals(jsonArray.get(1).getAsJsonObject().get("latest_message").getAsJsonObject().get("created_at").getAsString(), "2016-08-24T10:15:00.670Z");

        assertEquals(jsonArray.get(2).getAsJsonObject().get("id").getAsString(), "3");
        assertEquals(jsonArray.get(2).getAsJsonObject().get("latest_message").getAsJsonObject().get("id").getAsString(), "3");
        assertEquals(jsonArray.get(2).getAsJsonObject().get("latest_message").getAsJsonObject().get("body").getAsString(), "Hi!");
        assertEquals(jsonArray.get(2).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("id").getAsString(), "1");
        assertEquals(jsonArray.get(2).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("avatar_url").getAsString(), "http://placekitten.com/g/300/300");
        assertEquals(jsonArray.get(2).getAsJsonObject().get("latest_message").getAsJsonObject().get("created_at").getAsString(), "2016-08-23T10:15:00.670Z");

        assertEquals(jsonArray.get(3).getAsJsonObject().get("id").getAsString(), "4");
        assertEquals(jsonArray.get(3).getAsJsonObject().get("latest_message").getAsJsonObject().get("id").getAsString(), "4");
        assertEquals(jsonArray.get(3).getAsJsonObject().get("latest_message").getAsJsonObject().get("body").getAsString(), "Morning!");
        assertEquals(jsonArray.get(3).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("id").getAsString(), "5");
        assertEquals(jsonArray.get(3).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("avatar_url").getAsString(), "http://placekitten.com/g/304/304");
        assertEquals(jsonArray.get(3).getAsJsonObject().get("latest_message").getAsJsonObject().get("created_at").getAsString(), "2016-08-22T10:15:00.670Z");

        assertEquals(jsonArray.get(4).getAsJsonObject().get("id").getAsString(), "5");
        assertEquals(jsonArray.get(4).getAsJsonObject().get("latest_message").getAsJsonObject().get("id").getAsString(), "5");
        assertEquals(jsonArray.get(4).getAsJsonObject().get("latest_message").getAsJsonObject().get("body").getAsString(), "Pleep!");
        assertEquals(jsonArray.get(4).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("id").getAsString(), "6");
        assertEquals(jsonArray.get(4).getAsJsonObject().get("latest_message").getAsJsonObject().get("from_user").getAsJsonObject().get("avatar_url").getAsString(), "http://placekitten.com/g/305/305");
        assertEquals(jsonArray.get(4).getAsJsonObject().get("latest_message").getAsJsonObject().get("created_at").getAsString(), "2016-08-21T10:15:00.670Z");

    }

    @Test(expected = UnirestException.class)
    public void testFetchRecentConversationWithNoConnectionFailure() throws UnirestException {
        final RestClient restClientMock = mock(RestClient.class);
        when(restClientMock.doGetRequest(ConversationService.API_BASE_URL + "/conversations")).thenThrow(UnirestException.class);
        when(restClientMock.doGetRequest(ConversationService.API_BASE_URL + "/users")).thenThrow(UnirestException.class);

        final ConversationService conversationService = new ConversationService(restClientMock);

        conversationService.getRecentConversationSummaries();
    }

    @Test(expected = UnirestException.class)
    public void testFetchRecentConversationOnUserFetchFailure() throws UnirestException {
        final RestClient restClientMock = mock(RestClient.class);
        when(restClientMock.doGetRequest(ConversationService.API_BASE_URL + "/users")).thenThrow(UnirestException.class);

        final ConversationService conversationService = new ConversationService(restClientMock);

        conversationService.getRecentConversationSummaries();
    }

    @Test(expected = UnirestException.class)
    public void testFetchRecentConversationOnConversationFetchFailure() throws UnirestException {
        final RestClient restClientMock = mock(RestClient.class);
        when(restClientMock.doGetRequest(ConversationService.API_BASE_URL + "/conversations")).thenThrow(UnirestException.class);

        final ConversationService conversationService = new ConversationService(restClientMock);

        conversationService.getRecentConversationSummaries();
    }

    @Test(expected = UnirestException.class)
    public void testFetchRecentConversationOnMessageFetchFailure() throws UnirestException {
        final RestClient restClientMock = mock(RestClient.class);
        when(restClientMock.doGetRequest(ConversationService.API_BASE_URL + "/conversations/1/messages")).thenThrow(UnirestException.class);
        when(restClientMock.doGetRequest(ConversationService.API_BASE_URL + "/conversations")).thenReturn(restClient.doGetRequest(ConversationService.API_BASE_URL + "/conversations"));

        final ConversationService conversationService = new ConversationService(restClientMock);

        conversationService.getRecentConversationSummaries();
    }
}