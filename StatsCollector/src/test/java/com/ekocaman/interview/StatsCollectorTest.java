package com.ekocaman.interview;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatsCollectorTest {

    @Before
    public void setup() {

    }

    @Test
    public void testEstimatedMedianCalculationEmptySet() {
        StatsCollector statsCollector = new StatsCollector();
        final double median = statsCollector.getMedian();
        assertEquals(0, median, 0);
    }

    @Test
    public void testEstimatedMedianCalculationOneElement() {
        StatsCollector statsCollector = new StatsCollector();
        statsCollector.pushValue(5000);
        final double median = statsCollector.getMedian();
        assertEquals(5000, median, 1000);
    }

    @Test
    public void testEstimatedMedianCalculationTwoElement() {
        StatsCollector statsCollector = new StatsCollector();
        statsCollector.pushValue(5000);
        statsCollector.pushValue(10000);
        final double median = statsCollector.getMedian();
        assertEquals(7000, median, 1000);
    }

    @Test
    public void testEstimatedMedianCalculationInOrderedInputSuccessfully() {
        StatsCollector statsCollector = new StatsCollector();
        double[] responseTimes = new double[100];

        int index = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                responseTimes[index++] = (i + Math.random()) * 1000;
            }
        }

        for (int i = 0; i < responseTimes.length; i++) {
            statsCollector.pushValue(responseTimes[i]);
        }

        final double median = statsCollector.getMedian();
        assertEquals(5000, median, 1000);
    }

    @Test
    public void testEstimatedMedianCalculationUnorderedSuccessfully() {
        StatsCollector statsCollector = new StatsCollector();
        List<Double> responseTimes = new ArrayList<Double>();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                responseTimes.add((i + Math.random()) * 1000);
            }
        }
        Collections.shuffle(responseTimes);

        for (int i = 0; i < responseTimes.size(); i++) {
            statsCollector.pushValue(responseTimes.get(i));
        }

        final double median = statsCollector.getMedian();
        assertEquals(5000, median, 1000);
    }

    @Test
    public void testEstimatedMedianCalculationHugeDataSuccessfully() {
        StatsCollector statsCollector = new StatsCollector();
        for (int i = 0; i < 19; i++) {
            for (int j = 0; j < 1000000; j++) {
                statsCollector.pushValue((i + Math.random()) * 1000);
            }
        }

        final double median = statsCollector.getMedian();
        assertEquals(9000, median, 1000);
    }

    @Test
    public void testAverageCalculationEmptySet() {
        StatsCollector statsCollector = new StatsCollector();
        final double average = statsCollector.getAverage();
        assertEquals(0, average, 0);
    }

    @Test
    public void testAverageCalculationOneElement() {
        StatsCollector statsCollector = new StatsCollector();
        statsCollector.pushValue(5000);
        final double average = statsCollector.getAverage();
        assertEquals(5000, average, 1000);
    }

    @Test
    public void testAverageCalculationTwoElement() {
        StatsCollector statsCollector = new StatsCollector();
        statsCollector.pushValue(5000);
        statsCollector.pushValue(10000);
        final double average = statsCollector.getAverage();
        assertEquals(7500, average, 1000);
    }

    @Test
    public void testAverageCalculationHugeDataSuccessfully() {
        StatsCollector statsCollector = new StatsCollector();
        for (int i = 0; i < 19; i++) {
            for (int j = 0; j < 1000000; j++) {
                statsCollector.pushValue((i + Math.random()) * 1000);
            }
        }

        final double average = statsCollector.getAverage();
        assertEquals(9000, average, 1000);
    }

    @Test
    public void testEstimatedMedianCalculationAndAverageWithNegativeInput() {
        StatsCollector statsCollector = new StatsCollector();
        double[] responseTimes = new double[100];

        int index = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                responseTimes[index++] = -(i + Math.random()) * 1000;
            }
        }

        for (int i = 0; i < responseTimes.length; i++) {
            statsCollector.pushValue(responseTimes[i]);
        }

        final double median = statsCollector.getMedian();
        final double average = statsCollector.getAverage();
        assertEquals(0, median, 0);
        assertEquals(0, average, 0);
    }

    @Test
    public void testEstimatedMedianCalculationAndAverageWithTimeoutInput() {
        StatsCollector statsCollector = new StatsCollector();
        double[] responseTimes = new double[100];

        int index = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                responseTimes[index++] = (19 + Math.random()) * 1000;
            }
        }

        for (int i = 0; i < responseTimes.length; i++) {
            statsCollector.pushValue(responseTimes[i]);
        }

        final double median = statsCollector.getMedian();
        final double average = statsCollector.getAverage();
        assertEquals(0, median, 0);
        assertEquals(0, average, 0);
    }
}
