package com.ekocaman.interview;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/*
* Our application servers receive approximately 20 000
* http requests  per second. Response timeout is 19000ms.
* Implement a statistics collector that calculates the
* median and average request response times for a 7 day
* dataset.
*
* Assigment:
* 1. Implement StatsCollector
* 2. Write tests (below StatsCollector)
*/
public class StatsCollector {
    private static final int RESPONSE_TIMEOUT_IN_MILLIS = 19000;
    private static final int RESPONSE_TIMEOUT_IN_SECONDS = RESPONSE_TIMEOUT_IN_MILLIS / 1000;

    /**
     * Frequencies of each seconds
     */
    private HashMap<Integer, AtomicLong> frequencies = new HashMap<Integer, AtomicLong>();
    private AtomicLong totalInput = new AtomicLong();
    private double average = 0;

    public StatsCollector() {
        // Initialize frequencies as zero
        for (int i = 0; i < RESPONSE_TIMEOUT_IN_SECONDS; i++) {
            frequencies.put(i, new AtomicLong());
        }
    }

    public void pushValue(double responseTimeInMillis) {
        if (responseTimeInMillis > 0 && responseTimeInMillis < RESPONSE_TIMEOUT_IN_MILLIS) {
            BigDecimal totalSum = BigDecimal.valueOf(totalInput.get())
                    .multiply(BigDecimal.valueOf(average))
                    .add(BigDecimal.valueOf(responseTimeInMillis));

            average = totalSum.divide(BigDecimal.valueOf(totalInput.get() + 1), 3, RoundingMode.HALF_UP).doubleValue();

            final int responseTimeInSeconds = (int) (responseTimeInMillis / 1000);
            frequencies.get(responseTimeInSeconds).incrementAndGet();
            totalInput.incrementAndGet();
        }
    }

    /**
     * Calculate estimated median from grouped frequencies
     * https://www.mathsisfun.com/data/frequency-grouped-mean-median-mode.html
     *
     * @return
     */
    public double getMedian() {
        if (totalInput.get() <= 0) {
            return 0d;
        }

        final double medianElement = (double) totalInput.get() / 2;

        long minCumulativeFreq = 0;
        long maxCumulativeFreq = 0;
        long cumulativeFreq = 0;
        int freqGroup = 0;
        for (int i = 0; i < RESPONSE_TIMEOUT_IN_SECONDS; i++) {
            if (medianElement <= cumulativeFreq + frequencies.get(i).get()) {
                minCumulativeFreq = cumulativeFreq;
                maxCumulativeFreq = cumulativeFreq + frequencies.get(i).get();
                freqGroup = i;
                break;
            }
            cumulativeFreq += frequencies.get(i).get();
        }

        if (maxCumulativeFreq == minCumulativeFreq) {
            // Something wrong
            return 0;
        }

        return (((medianElement - minCumulativeFreq) / (maxCumulativeFreq - minCumulativeFreq)) + freqGroup) * 1000;
    }

    public double getAverage() {
        return average;
    }
}
